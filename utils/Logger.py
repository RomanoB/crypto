import logging

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


class Logger(logging.Logger):
    def __init__(self, *args, **kwargs):
        super(Logger, self).__init__(*args, **kwargs)
        _formatter = logging.Formatter(LOG_FORMAT)
        _ch = logging.StreamHandler()
        _ch.setFormatter(_formatter)
        self.addHandler(_ch)

    def do_log(self, level, message):
        def wrap(func):
            def wrapped(*args):
                if level == logging.DEBUG:
                    self.debug(message)
                elif level == logging.INFO:
                    self.info(message)
                elif level == logging.WARNING:
                    self.warning(message)
                elif level == logging.ERROR:
                    self.error(message)
                elif level == logging.FATAL:
                    self.fatal(message)
                else:
                    raise NotImplemented('log level {} not implemented'.format(level))

                func(*args)

            return wrapped
        return wrap


if __name__ == '__main__':
    logger = Logger('test')
    @logger.do_log(logging.WARNING, 'mijn test')
    def yolo(a, b):
        print(a, b)

    yolo('hi','ha')