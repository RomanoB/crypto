from .markets import Markets
from .clients import bittrex, livecoin, kraken, poloniex, bitstamp


#ToDo: https://bx.in.th/info/api/ -- https://www.binance.com - https://www.bithumb.com/u1/US127


class ClientFactory:
    __builder = {Markets.BITTREX  : [bittrex.Client, (Markets.BITTREX, "https://bittrex.com/")],
                 Markets.LIVECOIN : [livecoin.Client, (Markets.LIVECOIN, "https://api.livecoin.net/")],
                 Markets.KRAKEN   : [kraken.Client, (Markets.KRAKEN, "https://api.kraken.com/")],
                 Markets.POLONIEX : [poloniex.Client, (Markets.POLONIEX, "https://poloniex.com/")],
                 Markets.BITSTAMP : [bitstamp.Client, (Markets.BITSTAMP, "https://www.bitstamp.net/")]}

    @staticmethod
    def create_client(market, pair, loop):
        client, args = ClientFactory.__builder.get(market)
        return client(*args, pair=pair, loop=loop)
