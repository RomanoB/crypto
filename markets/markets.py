from enum import Enum

class Markets(Enum):
    BITTREX = 1
    LIVECOIN = 2
    KRAKEN = 3
    POLONIEX = 4
    BITSTAMP = 5
