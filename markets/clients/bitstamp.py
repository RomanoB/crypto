from .base import AsyncBaseClient
from .base import OrderBook
from .base.Exceptions import InvalidTraidingPair

"""
Work in Progress
https://www.bitstamp.net/
"""


class Client(AsyncBaseClient):
    GET_ORDERBOOK = "api/v2/order_book/{}{}/"

    def __init__(self, host, market, pair, loop):
        super(Client, self).__init__(host, market, loop)
        self.pair = pair


    async def get_orderbook(self):
        raw = await self.async_get(Client.GET_ORDERBOOK.format(self.pair.sell.name,
                                                               self.pair.buy.name))
        if self._is_valid(raw):
            parsed = self._parse_and_transform(raw)
            return OrderBook(self.market, self.pair, parsed)

    def _is_valid(self, raw):
        if raw is None:
            raise InvalidTraidingPair('{} --> {}'.format(self.market.name, self.pair))
        return True

    def _parse_and_transform(self, raw):
        return {'asks' : self._pre_process(raw['asks']),
                'bids' : self._pre_process(raw['bids'])}

    @staticmethod
    def _pre_process(elements):
        return [{'Quantity': float(elem[1]), 'Rate': float(elem[0])} for elem in elements]

