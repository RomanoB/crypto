from abc import ABC, abstractmethod
import requests
import logging
import aiohttp


class AsyncBaseClient(ABC):
    def __init__(self, market, host, loop):
        self._market = market
        self._host = host
        self._session = aiohttp.ClientSession(loop=loop)
        self._logger = logging.getLogger(name=self._market.name)

    def warn(self, response, uri):
        self._logger.warning("{} {} {}".format(response.status, response.reason, self._host + uri))

    async def async_get(self, uri):
        async with self._session.get(url=self._host + uri, timeout=5) as response:
            if response.status != 200:
                self.warn(response, uri)

            try:
                return await response.json()
            except:
                return None

    @abstractmethod
    def get_orderbook(self):
        pass

    @property
    def market(self):
        return self._market

    def __del__(self):
        self._session.close()

class SynchronousBaseClient(ABC):
    def __init__(self, market, host, ssl=True, pool_connections=1, pool_maxsize=1):
        self._market = market
        self._host = host
        self._session = self._init_session(ssl, pool_connections, pool_maxsize)
        self._logger = logging.getLogger(self._market)

    def _init_session(self, ssl, pool_connections, pool_maxsize):
        session = requests.Session()
        protocol = 'https://' if ssl else 'http://'
        session.mount(protocol, requests.adapters.HTTPAdapter(pool_connections=pool_connections,
                                                              pool_maxsize=pool_maxsize))
        return session

    @property
    def market(self):
        return self._market

    def get(self, uri):
        self._logger.debug('Getting: {}'.format(self._host + uri))
        response = self._session.get(self._host + uri)
        assert response.status_code == 200
        return response.json()

    @abstractmethod
    def get_orderbook(self):
        pass
