from abc import ABC, abstractmethod
import time


class AbstractOrderBook(ABC):
    @property
    @abstractmethod
    def market(self):
        pass

    @property
    @abstractmethod
    def pair(self):
        pass

    @property
    @abstractmethod
    def timestamp(self):
        pass

    @property
    @abstractmethod
    def asks(self):
        pass

    @property
    @abstractmethod
    def bids(self):
        pass

    @abstractmethod
    def bid_volume_above_price(self, price):
        pass

    @abstractmethod
    def ask_volume_above_price(self, price):
        pass

    @abstractmethod
    def min_bid(self):
        pass

    @property
    @abstractmethod
    def min_ask(self):
        pass

    @staticmethod
    @abstractmethod
    def _min(ask_or_bid):
        pass

    @property
    @abstractmethod
    def max_bid(self):
        pass

    @property
    @abstractmethod
    def max_ask(self):
        return self._max(self.asks)

    @staticmethod
    @abstractmethod
    def _max(ask_or_bid):
        pass


class OrderBook(AbstractOrderBook):
    def __init__(self, market, pair, raw):
        self._raw = raw
        self._pair = pair
        self._market = market
        self._timestamp = int(time.time())

    @property
    def market(self):
        return self._market

    @property
    def pair(self):
        return self._pair

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def asks(self):
        return self._raw.get('asks')

    @property
    def bids(self):
        return self._raw.get('bids')

    def bid_volume_above_price(self, price):
        return self._volume_above_price(price, self.bids)

    def ask_volume_above_price(self, price):
        return self._volume_above_price(price, self.asks)

    def _volume_above_price(self, price, asks_or_bids):
        return sum([float(volume) for value, volume in asks_or_bids if float(value) >= price])

    @property
    def min_bid(self):
        return self._min(self.bids)

    @property
    def min_ask(self):
        return self._min(self.asks)

    @staticmethod
    def _min(ask_or_bid):
        return min(ask_or_bid, key=lambda elem: elem['Rate'])

    @property
    def max_bid(self):
        return self._max(self.bids)

    @property
    def max_ask(self):
        return self._max(self.asks)

    @staticmethod
    def _max(ask_or_bid):
        return max(ask_or_bid, key=lambda elem: elem['Rate'])
