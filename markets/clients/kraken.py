from .base import AsyncBaseClient
from .base import OrderBook
from .base.Exceptions import InvalidTraidingPair


class Client(AsyncBaseClient):
    GET_ORDERBOOK = "0/public/Depth?pair={}{}"

    def __init__(self, host, market, pair, loop):
        super(Client, self).__init__(host, market, loop)
        self.pair = pair
        self._pair_names = self._translate_pair(pair)

    async def get_orderbook(self):
        raw = await self.async_get(Client.GET_ORDERBOOK.format(self._pair_names[0],self._pair_names[1]))
        if self._is_valid(raw):
            parsed = self._parse_and_transform(raw)
            return OrderBook(self.market, self.pair, parsed)

    def _is_valid(self, raw):
        if 'EQuery:Unknown asset pair' in raw.get('error', []):
            raise InvalidTraidingPair('{} --> {}'.format(self.market.name, self.pair))

        return True

    def _parse_and_transform(self, raw):
        parsed = dict()
        parsed['asks'] = self._pre_process(list(raw['result'].values())[0]['asks'])
        parsed['bids'] = self._pre_process(list(raw['result'].values())[0]['bids'])
        return parsed

    @staticmethod
    def _pre_process(elems):
        return [{'Quantity': float(elem[1]), 'Rate': float(elem[0])} for elem in elems]

    @staticmethod
    def _translate_pair(pair):
        f = lambda x: 'XBT' if x.name == 'BTC' else x.name
        return [f(currency) for currency in pair]