from .base import AsyncBaseClient
from .base import OrderBook
from .base.Exceptions import InvalidTraidingPair


class Client(AsyncBaseClient):
    GET_ORDERBOOK = "api/v1.1/public/getorderbook?market={}-{}&type=both"

    def __init__(self, host, market, pair, loop):
        super(Client, self).__init__(host, market, loop)
        self.pair = pair

    async def get_orderbook(self):
        raw = await self.async_get(Client.GET_ORDERBOOK.format(self.pair.sell.name, self.pair.buy.name))
        if self._is_valid(raw):
            parsed = self._parse_and_transform(raw)
            return OrderBook(self.market, self.pair, parsed)

    def _is_valid(self, raw):
        if raw.get('message') == 'INVALID_MARKET':
            raise InvalidTraidingPair('{} --> {}'.format(self.market.name, self.pair))
        return True

    def _parse_and_transform(self, raw):
        parsed = dict()
        parsed['asks'] = raw.get('result', {}).get('sell')
        parsed['bids'] = raw.get('result', {}).get('buy')
        return parsed

