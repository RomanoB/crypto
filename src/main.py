import asyncio
import logging

from markets import Markets
from utils.currencies import Crypto, Fiat, Pair
from dispatcher import Dispatcher, Callbacks

# suppress requests lib
logging.getLogger('requests').setLevel(logging.WARN)

# set global log config
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(name)s: %(message)s',
                    datefmt='%a %d %b %Y %H:%M:%S')


def run(pairs, markets):
    loop = asyncio.get_event_loop()
    dispatcher = Dispatcher(pairs, markets, loop, interval_sec=2)
    dispatcher.register_callback(Callbacks.get_bid_and_ask)
    asyncio.gather(dispatcher.run())
    dispatcher.run_forever()

if __name__ == '__main__':
    # select markets
    markets = [Markets.BITTREX, Markets.KRAKEN,
               Markets.LIVECOIN, Markets.POLONIEX,
               Markets.BITSTAMP]

    # select Pairs
    pairs = [Pair(Crypto.BTC, Crypto.ETH),
             Pair(Crypto.BTC, Fiat.USD),
             Pair(Crypto.BTC, Crypto.XRP)]

    # runs forever
    run(pairs, markets)
