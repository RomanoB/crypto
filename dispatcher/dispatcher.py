import json
import logging
import asyncio
import random
import itertools

from asyncio import TimeoutError
from markets import ClientFactory
from collections import defaultdict
from markets.clients.base.Exceptions import InvalidTraidingPair
from aiohttp.client_exceptions import ClientConnectorError, ContentTypeError


class Dispatcher(object):
    def __init__(self, pairs, markets, loop, interval_sec=5):
        self._markets = markets
        self._loop = loop
        self._pairs = pairs
        self._clients = None
        self._logger = logging.getLogger(__name__)
        self._bad_markets = list()
        self._skip_markets = defaultdict(int)
        self._callbacks = []
        self._should_initialize_clients = True
        self._interval_sec = interval_sec

    async def _get_orderbooks(self, client):
        try:

            orderbook = await client.get_orderbook()

        except InvalidTraidingPair as e:
            self._logger.warning('{}: {}'.format(type(e).__name__, e))
            self._bad_markets.append(client.market)
            self._purge_client(client)

        except TimeoutError as e:
            self._logger.warning('{} on {} --> {} {} '.format(type(e).__name__, client.market, client.pair,
                                                              'skipping next iteration.'))
            self._skip_markets[client.market] += 1

        except ClientConnectorError as e:
            self._logger.warning('{}: {} '.format(type(e).__name__, 'retrying in a minute.'))
            self._skip_markets[client.market] += 30

        except ContentTypeError as e:
            self._logger.warning('{}: {}'.format(type(e).__name__, e))
            self._logger.warning('{}: {} '.format(type(e).__name__, 'retrying soon..'))
            self._skip_markets[client.market] += 5

        except Exception as e:
            self._logger.warning('{}: {}'.format(type(e).__name__, e))
            self._bad_markets.append(client.market)
            self._purge_client(client)
        else:
            return orderbook

    def _purge_client(self, client):
        for i, _client in enumerate(self._clients):
            if id(_client) == id(client):
                self._logger.warning('killing client {}  --> {}'.format(self._clients[i].market.name,
                                                                       self._clients[i].pair))
                del self._clients[i]
                break

    def _update_skip_markets(self):
        for client in self._clients:
            if self._skip_markets[client.market] > 0:
                self._skip_markets[client.market] -= 1

    def run_forever(self):
        self._loop.run_forever()

    def register_callback(self, callback):
        self._callbacks.append(callback)

    async def fetch(self):
        self._maybe_initialize_clients()
        tasks = self._get_tasks()
        self._update_skip_markets()
        return filter(lambda x: x is not None, await asyncio.gather(*tasks))

    def _do_callbacks(self, orderbooks):
        for callback in self._callbacks:
            orderbooks = callback(orderbooks)
        return orderbooks

    def print_orderbooks(self, future):
        orderbooks = future.result()
        orderbooks = self._do_callbacks(orderbooks)
        for orderbook in orderbooks:
            print(orderbook[1], '-->', orderbook[0], '-->', orderbook[2], '--', orderbook[3])

    def to_json(self, future):
        orderbooks = future.result()
        orderbooks = self._do_callbacks(orderbooks)
        for orderbook in orderbooks:
            with open('{}-{}.json'.format(orderbook[0], random.randint(0, 9)), 'w') as f:
                json.dump(orderbook[2]._raw, f)

    async def run(self):
        while True:
            task = self._loop.create_task(self.fetch())
            task.add_done_callback(self.print_orderbooks)
            await asyncio.sleep(self._interval_sec)
            while len(asyncio.Task.all_tasks()) > len(self._pairs) * 2:
                await asyncio.sleep(0.5)

    def _get_tasks(self):
        tasks = [self._get_orderbooks(client) for client in self._clients
                 if not self._skip_markets[client.market] > 0]
        return tasks

    def _maybe_initialize_clients(self):
        if not self._should_initialize_clients: return
        try:
            self._logger.info('Initializing clients.')
            self._clients = list(itertools.chain(*[[ClientFactory.create_client(market, pair, self._loop)
                                                   for market in self._markets if market not in self._bad_markets]
                                                   for pair in self._pairs]))
        except Exception as e:
            self._logger.error(e)
        else:
            self._should_initialize_clients = False



