import itertools


class Callbacks:
    @staticmethod
    def get_diffs(orderbooks):
        return [(a.market, b.market, a.min_ask['Rate'] - b.max_bid['Rate'])
                for a, b in itertools.combinations(orderbooks, 2)]

    @staticmethod
    def get_bid_and_ask(orderbooks):
        return [(orderbook.market.name, orderbook.pair,
                 orderbook.min_ask['Rate'], orderbook.max_bid['Rate'])
                for orderbook in orderbooks]

    @staticmethod
    def get_raw(orderbooks):
        return [(orderbook.market.name, orderbook.pair, orderbook) for orderbook in orderbooks]

